<?php



require_once './vendor/autoload.php';


use labib\Bangla\{Audio,Video as BanglaV};
use labib\English\Video as EnglishV;
use labib\Turkish\Video as turk;



$banVid = new BanglaV;
$engVid = new EnglishV;
$banAud = new Audio;
$turkVid = new turk;